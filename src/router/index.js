import Vue from 'vue';
import VueRouter from 'vue-router';
import firebase from 'firebase/app';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    meta: { auth: true, layout: 'main' },
    component: () => import('../views/home/Home.vue')
  },
  {
    path: '/login',
    name: 'Login',
    meta: { layout: 'auth' },
    component: () => import('../views/auth/Login.vue')
  },
  {
    path: '/signup',
    name: 'Register',
    meta: { layout: 'auth' },
    component: () => import('../views/auth/Register.vue')
  },
  {
    path: '/recovery',
    name: 'Recovery',
    meta: { layout: 'auth' },
    component: () => import('../views/auth/Recovery.vue')
  },
  {
    path: '/profile-edit',
    name: 'ProfileEdit',
    meta: { auth: true, layout: 'main' },
    component: () => import('../views/profile/ProfileEdit.vue')
  },
  {
    path: '/call/:roomId',
    name: 'VideoCall',
    meta: { auth: true, layout: 'main' },
    component: () => import('../views/VideoCall.vue')
  },
  {
    path: '*',
    component: () => import('../views/NotFound.vue')
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  const currentUser = firebase.auth().currentUser;
  const requireAuth = to.matched.some(record => record.meta.auth);

  if (requireAuth && !currentUser) {
    next('/login');
  } else {
    next();
  }
});

export default router;
