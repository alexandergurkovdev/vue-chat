import firebase from 'firebase/app';

export default {
  namespaced: true,
  state: {
    users: []
  },
  actions: {
    async getUsers({ commit }, currentUser) {
      try {
        await firebase
          .database()
          .ref('users')
          .on('child_added', snapshot => {
            if (currentUser.uid !== snapshot.key) {
              let user = snapshot.val();
              user['uid'] = snapshot.key;
              user['status'] = 'offline';
              commit('setUsers', user);
            }
          });

        await firebase
          .database()
          .ref('.info/connected')
          .on('value', async snapshot => {
            if (snapshot.val() === true && currentUser) {
              let ref = await firebase
                .database()
                .ref('presence')
                .child(currentUser.uid);

              ref.set(true);
              ref.onDisconnect().remove();

              window.onunload = () => {
                ref.remove();
              };
            }
          });
      } catch (error) {
        commit('setError', error.message);
        throw error;
      }
    },
    listenUerStatuses({ commit }, currentUser) {
      const presenceRef = firebase.database().ref('presence');

      presenceRef.on('child_added', snapshot => {
        if (currentUser.uid !== snapshot.key) {
          commit('addStatusToUser', { userId: snapshot.key });
        }
      });

      presenceRef.on('child_removed', snapshot => {
        if (currentUser.uid !== snapshot.key) {
          commit('addStatusToUser', { userId: snapshot.key, connected: false });
        }
      });
    }
  },
  mutations: {
    addStatusToUser(state, { userId, connected = true }) {
      let index = state.users.findIndex(user => user.uid === userId);
      if (index !== -1) {
        connected === true ? (state.users[index].status = 'online') : (state.users[index].status = 'offline');
      }
    },
    setUsers(state, user) {
      state.users.push(user);
    }
  },
  getters: {
    users(state) {
      return state.users;
    }
  }
};
