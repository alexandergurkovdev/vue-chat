import firebase from 'firebase/app';
import router from '@/router';

export default {
  namespaced: true,
  state: {
    currentUser: null,
    loading: false,
    error: null
  },
  actions: {
    async loginWithGoogle({ commit, dispatch }, locale) {
      commit('setLoading', true);
      await firebase
        .auth()
        .signInWithPopup(new firebase.auth.GoogleAuthProvider())
        .then(async response => {
          commit('setCurrentUser', response.user);
          if (!response.user.emailVerified) {
            await response.user.sendEmailVerification();
          }
          dispatch('createUserReference', { user: response.user, locale });
          commit('setLoading', false);
          router.push('/');
        })
        .catch(error => {
          commit('setLoading', false);
          commit('setError', error.message);
          throw error;
        });
    },
    async loginWithFacebook({ commit, dispatch }, locale) {
      commit('setLoading', true);
      await firebase
        .auth()
        .signInWithPopup(new firebase.auth.FacebookAuthProvider())
        .then(async response => {
          commit('setCurrentUser', response.user);
          if (!response.user.emailVerified) {
            await response.user.sendEmailVerification();
          }
          dispatch('createUserReference', { user: response.user, locale });
          commit('setLoading', false);
          router.push('/');
        })
        .catch(error => {
          commit('setLoading', false);
          commit('setError', error.message);
          throw error;
        });
    },
    async createUserReference({ commit }, { user, locale }) {
      try {
        await firebase
          .database()
          .ref(`/users/${user.uid}/`)
          .set({
            uid: user.uid,
            name: user.displayName,
            email: user.email,
            avatar: user.photoURL,
            emailVerified: user.emailVerified
          });

        await firebase
          .firestore()
          .collection('locales')
          .doc(user.uid)
          .set({
            locale: locale
          });
      } catch (error) {
        commit('setError', error.message);
        throw error;
      }
    },
    async signUp({ commit, dispatch }, { displayName, email, password, locale }) {
      try {
        commit('setLoading', true);

        await firebase.auth().createUserWithEmailAndPassword(email, password);

        const user = firebase.auth().currentUser;

        user.updateProfile({
          displayName: displayName
        });

        await user.sendEmailVerification();
        await dispatch('createUserReference', { user, locale });

        commit('setCurrentUser', user);
        commit('setLoading', false);
        router.push('/');
      } catch (error) {
        commit('setLoading', false);
        commit('setError', error.message);
        throw error;
      }
    },
    async signIn({ commit }, { email, password }) {
      try {
        commit('setLoading', true);

        await firebase.auth().signInWithEmailAndPassword(email, password);
        const user = firebase.auth().currentUser;

        commit('setCurrentUser', user);
        commit('setLoading', false);
        router.push('/');
      } catch (error) {
        commit('setLoading', false);
        commit('setError', error.message);
        throw error;
      }
    },
    async updateProfile({ commit }, payload) {
      try {
        commit('setLoading', true);
        const user = firebase.auth().currentUser;
        await firebase
          .database()
          .ref(`/users/${user.uid}/`)
          .update({
            name: payload.displayName,
            email: payload.email
          });

        if (payload.displayName) {
          user.updateProfile({
            displayName: payload.displayName
          });
        }
        if (payload.email !== user.email) {
          user
            .updateEmail(payload.email)
            .then(async function() {
              await user.sendEmailVerification();
            })
            .catch(function(error) {
              console.log(error.message);
            });
        }

        if (payload.password.length > 0) {
          await user.updatePassword(payload.password);
        }
        commit('setLoading', false);
      } catch (error) {
        commit('setLoading', false);
        commit('setError', error.message);
        throw error;
      }
    },
    async recoveryPassword({ commit }, { email }) {
      try {
        commit('setLoading', true);
        await firebase.auth().sendPasswordResetEmail(email);
        commit('setLoading', false);
        router.push('/login');
      } catch (error) {
        commit('setLoading', false);
        commit('setError', error.message);
        throw error;
      }
    },
    async deleteAccount({ commit, dispatch }) {
      try {
        commit('setLoading', true);
        const user = firebase.auth().currentUser;
        await firebase
          .database()
          .ref(`users/${user.uid}`)
          .remove();

        await firebase
          .firestore()
          .collection('locales')
          .doc(user.uid)
          .delete();

        await firebase
          .database()
          .ref('presence')
          .child(user.uid)
          .remove();

        await dispatch('logout', user);
        await user.delete();
      } catch (error) {
        commit('setLoading', false);
        commit('setError', error.message);
        throw error;
      }
    },
    async logout({ commit }, currentUser) {
      await firebase
        .database()
        .ref('presence')
        .child(currentUser.uid)
        .ref.remove();
      await firebase.auth().signOut();
      commit('init');
      router.push('/login');
    }
  },
  mutations: {
    setCurrentUser(state, user) {
      state.currentUser = user;
    },
    setLoading(state, loading) {
      state.loading = loading;
    },
    setError(state, error) {
      state.error = error;
    },
    init(state) {
      state.error = null;
      state.loading = false;
    }
  },
  getters: {
    loading(state) {
      return state.loading;
    },
    error(state) {
      return state.error;
    },
    currentUser(state) {
      return state.currentUser;
    }
  }
};
