import Vue from 'vue';
import Vuex from 'vuex';
import firebase from 'firebase/app';
import ui from './modules/ui';
import auth from './modules/auth';
import users from './modules/users';
import chats from './modules/chats';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    loading: false,
    locale: undefined
  },
  actions: {
    async changeLocale({ commit }, value) {
      const user = firebase.auth().currentUser;

      commit('setLoading', true);

      if (user) {
        await firebase
          .firestore()
          .collection('locales')
          .doc(user.uid)
          .set({
            locale: value
          });
      } else {
        commit('setLocale', value);
      }

      commit('setLoading', false);
    },
    async getLocale({ commit }) {
      const user = firebase.auth().currentUser;
      commit('setLoading', true);
      if (user) {
        await firebase
          .firestore()
          .collection('locales')
          .doc(user.uid)
          .onSnapshot(doc => {
            if (doc.data()) {
              commit('setLocale', doc.data().locale);
            }
          });
      } else {
        commit('setLocale', 'en-US');
      }
      commit('setLoading', false);
    },
    detachListeners() {
      let unsubscribeMessages = firebase
        .firestore()
        .collection('messages')
        .onSnapshot(function() {});

      let unsubscribeChats = firebase
        .firestore()
        .collection('chats')
        .onSnapshot(function() {});

      firebase
        .database()
        .ref('users')
        .off();

      firebase
        .database()
        .ref('presence')
        .off();

      unsubscribeMessages();
      unsubscribeChats();
    }
  },
  mutations: {
    setLocale(state, payload) {
      state.locale = payload;
    },
    setLoading(state, loading) {
      state.loading = loading;
    }
  },
  getters: {
    locale(state) {
      return state.locale;
    },
    loading(state) {
      return state.loading;
    }
  },
  modules: { ui, auth, users, chats }
});
