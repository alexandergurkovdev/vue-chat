import firebase from 'firebase/app';
import { v4 as uuidv4 } from 'uuid';

export default {
  namespaced: true,
  state: {
    loading: false,
    error: null,
    chats: [],
    messages: [],
    notifCount: [],
    currentChat: null,
    chatExists: undefined,
    term: '',
    filter: 'all',
    dateFilter: null
  },
  actions: {
    // chats actions
    async addChat({ commit, dispatch }, { user, chatId, message }) {
      const currentUser = firebase.auth().currentUser;
      try {
        commit('setLoading', true);
        const chat = {
          id: chatId,
          name: user.name,
          users: [currentUser.uid, user.uid],
          callIsReady: true
        };
        await firebase
          .firestore()
          .collection('chats')
          .doc(chatId)
          .set(chat);

        await dispatch('sendMessage', {
          message,
          chatId,
          user: currentUser,
          messageType: 'text'
        });

        commit('setCurrentChat', chatId);
        commit('setLoading', false);
      } catch (error) {
        commit('setLoading', false);
        commit('setError', error.message);
        throw error;
      }
    },
    async deleteChat({ commit, state }, chatId) {
      try {
        commit('setLoading', true);
        if (state.currentChat === chatId) {
          await firebase
            .firestore()
            .collection('chats')
            .doc(chatId)
            .delete();
          await firebase
            .firestore()
            .collection('messages')
            .doc(chatId)
            .delete();

          await firebase
            .storage()
            .ref()
            .child(`chats/${chatId}`)
            .listAll()
            .then(function(result) {
              result.items.forEach(function(file) {
                file.delete();
              });
            })
            .catch(function(error) {
              console.log(error.message);
            });
          commit('setCurrentChat', null);
          commit('setMessages', []);
        }
        commit('setLoading', false);
      } catch (error) {
        commit('setLoading', false);
        commit('setError', error.message);
        throw error;
      }
    },
    async chatExists({ commit }, chatId) {
      const res = await firebase
        .firestore()
        .collection('chats')
        .doc(chatId)
        .get();

      if (res.data()) {
        commit('setChatExists', true);
      } else {
        commit('setChatExists', false);
      }
    },
    async getChats({ commit }) {
      try {
        commit('setLoading', true);

        const currentUser = firebase.auth().currentUser;
        await firebase
          .firestore()
          .collection('chats')
          .where('users', 'array-contains', currentUser.uid)
          .onSnapshot(querySnapshot => {
            let chats = [];
            querySnapshot.forEach(function(doc) {
              chats.push(doc.data());
            });
            commit('setChats', chats);
          });
        commit('setLoading', false);
      } catch (error) {
        commit('setLoading', false);
        commit('setError', error.message);
        throw error;
      }
    },
    // messages actions
    async sendMessage({ commit }, { message, chatId, user, messageType, file, isVideo }) {
      let newMessage;
      if (file) {
        newMessage = {
          id: uuidv4(),
          message,
          messageType,
          file: file ? file : null,
          timestamp: firebase.firestore.Timestamp.fromDate(new Date()),
          sender: {
            name: user.displayName,
            avatar: user.photoURL,
            uid: user.uid
          },
          isVideo: isVideo && isVideo === true ? true : false
        };
      } else {
        newMessage = {
          id: uuidv4(),
          message,
          messageType,
          timestamp: firebase.firestore.Timestamp.fromDate(new Date()),
          sender: {
            name: user.displayName,
            avatar: user.photoURL,
            uid: user.uid
          },
          isVideo: isVideo && isVideo === true ? true : false
        };
      }
      try {
        const res = await firebase
          .firestore()
          .collection('messages')
          .doc(chatId)
          .get();

        if (!res.data()) {
          await firebase
            .firestore()
            .collection('messages')
            .doc(chatId)
            .set({
              messages: [newMessage],
              users: chatId.split('|')
            });
        } else {
          await firebase
            .firestore()
            .collection('messages')
            .doc(chatId)
            .update({
              messages: [...res.data().messages, newMessage],
              users: chatId.split('|')
            });
        }
      } catch (error) {
        commit('setError', error.message);
        throw error;
      }
    },
    async listenMessages({ commit, state }, chatId) {
      await firebase
        .firestore()
        .collection('messages')
        .doc(chatId)
        .onSnapshot(doc => {
          if (doc.data()) {
            if (state.currentChat === chatId) {
              commit('setMessages', doc.data().messages);
            }
          }
        });
    },
    async deleteMessage({ commit }, { chatId, messageId, file }) {
      try {
        const res = await firebase
          .firestore()
          .collection('messages')
          .doc(chatId)
          .get();

        if (file) {
          firebase
            .storage()
            .ref()
            .child(file.filePath)
            .delete()
            .then(function() {
              console.log('file succesfully deleted');
            })
            .catch(function(error) {
              console.log(error);
            });
        }

        const prevMessages = res.data().messages;
        const newMessages = prevMessages.filter(message => message.id !== messageId);
        await firebase
          .firestore()
          .collection('messages')
          .doc(chatId)
          .set({
            messages: newMessages,
            users: chatId.split('|')
          });
      } catch (error) {
        commit('setError', error.message);
        throw error;
      }
    },
    async editMessage({ commit }, { chatId, messageId, newMessage }) {
      try {
        commit('setLoading', true);
        const res = await firebase
          .firestore()
          .collection('messages')
          .doc(chatId)
          .get();

        const messages = res.data().messages;
        const index = messages.findIndex(message => message.id === messageId);
        messages[index].message = newMessage;
        messages[index].edited = true;
        await firebase
          .firestore()
          .collection('messages')
          .doc(chatId)
          .update({
            messages
          });
        commit('setLoading', false);
      } catch (error) {
        commit('setLoading', false);
        commit('setError', error.message);
        throw error;
      }
    },
    // get messages for notifications
    async fetchAllRoomsMessages({ dispatch }, userId) {
      await firebase
        .firestore()
        .collection('messages')
        .where('users', 'array-contains', userId)
        .onSnapshot(snapshot => {
          let chatId;
          snapshot.forEach(doc => {
            const chatIdToArray = doc.id.split('|');
            const index = chatIdToArray.findIndex(id => id === userId);
            if (index !== -1) {
              chatId = chatIdToArray.join('|');
              dispatch('handleNotifications', { chatId, doc });
            }
          });
        });
    },
    handleNotifications({ state, commit }, { chatId, doc }) {
      let lastTotal = 0;
      let index = state.notifCount.findIndex(el => el.id === chatId);

      if (index !== -1) {
        if (chatId !== state.currentChat) {
          lastTotal = state.notifCount[index].total;
          if (doc.data().messages.length - lastTotal > 0) {
            state.notifCount[index].notif = doc.data().messages.length - lastTotal;
          }
          state.notifCount[index].lastKnownTotal = doc.data().messages.length;
        } else {
          state.notifCount[index].total = doc.data().messages.length;
          state.notifCount[index].lastKnownTotal = doc.data().messages.length;
          state.notifCount[index].notif = 0;
        }
      } else {
        commit('setNotifCount', {
          id: chatId,
          total: doc.data().messages.length,
          lastKnownTotal: doc.data().messages.length,
          notif: 0
        });
      }
    }
  },
  mutations: {
    setNotifCount(state, notifCount) {
      state.notifCount.push(notifCount);
    },
    setChats(state, chats) {
      state.chats = chats;
    },
    setCurrentChat(state, chatId) {
      state.currentChat = chatId;
    },
    setMessages(state, messages) {
      state.messages = messages;
    },
    setLoading(state, loading) {
      state.loading = loading;
    },
    setError(state, error) {
      state.error = error;
    },
    setChatExists(state, payload) {
      state.chatExists = payload;
    },
    resetNotifications(state, channelId) {
      let index = state.notifCount.findIndex(el => el.id === channelId);

      if (index !== -1) {
        state.notifCount[index].total = state.notifCount[index].lastKnownTotal;
        state.notifCount[index].notif = 0;
      }
    },
    search(state, payload) {
      state.term = payload;
    },
    filter(state, payload) {
      state.filter = payload;
    },
    setDateFilter(state, payload) {
      state.dateFilter = payload;
    }
  },
  getters: {
    chats(state) {
      return state.chats;
    },
    messages(state) {
      const checkTerm = item => item.message.toLowerCase().indexOf(state.term.toLowerCase()) > -1;

      const checkDate = item => {
        const itemDate = item.timestamp.seconds * 1000 + item.timestamp.nanoseconds / 1000000;
        if (state.dateFilter[0] === state.dateFilter[1]) {
          return (
            new Date(itemDate).getDate() === new Date(state.dateFilter[0]).getDate() &&
            new Date(itemDate).getMonth() === new Date(state.dateFilter[0]).getMonth()
          );
        } else {
          return itemDate > state.dateFilter[0] - 86400000 && itemDate < state.dateFilter[1] + 86400000;
        }
      };

      const checkFilter = item => {
        if ((state.filter === 'all' || state.filter === 'text') && state.term) {
          return item.messageType === 'text' && !item.isVideo && checkTerm(item);
        } else if (state.filter === 'text' && !state.term) {
          return item.messageType === 'text' && !item.isVideo;
        } else if (state.filter === 'document') {
          return item.messageType === 'document';
        } else if (state.filter === 'video') {
          return item.messageType === 'video' || item.isVideo;
        } else if (state.filter === 'voice') {
          return item.messageType === 'voice' || item.messageType === 'audio';
        } else if (state.filter === 'image') {
          return item.messageType === 'image';
        } else {
          return state.messages;
        }
      };
      if (state.dateFilter) {
        return state.messages.filter(item => checkDate(item) && checkFilter(item));
      } else {
        return state.messages.filter(item => checkFilter(item));
      }
    },
    notifCount(state) {
      return state.notifCount;
    },
    currentChat(state) {
      return state.currentChat;
    },
    loading(state) {
      return state.loading;
    },
    error(state) {
      return state.error;
    },
    dateFilter(state) {
      return state.dateFilter;
    }
  }
};
