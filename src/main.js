import Vue from 'vue';
import firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/firestore';
import 'firebase/auth';
import 'firebase/storage';
import router from './router';
import store from './store';
import localizeFilter from './filters/localize.filter';
import VueChatScroll from 'vue-chat-scroll';
import linkify from 'vue-linkify';
import moment from 'vue-moment';
import VuePlyr from 'vue-plyr';
import WebRTC from 'vue-webrtc';
import * as io from 'socket.io-client';
import { LMap, LTileLayer, LMarker } from 'vue2-leaflet';
import {
  Avatar,
  Dialog,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Input,
  Select,
  Option,
  Button,
  Popover,
  Tooltip,
  Form,
  FormItem,
  Tag,
  Alert,
  Icon,
  Row,
  Col,
  Progress,
  Link,
  Divider,
  Image,
  Loading,
  Notification,
  Popconfirm,
  DatePicker
} from 'element-ui';
import lang from 'element-ui/lib/locale/lang/en';
import locale from 'element-ui/lib/locale';
import 'element-ui/lib/theme-chalk/reset.css';
import 'element-ui/lib/theme-chalk/index.css';
import 'leaflet/dist/leaflet.css';
import App from './App.vue';

Vue.use(VuePlyr, {
  plyr: {
    fullscreen: { enabled: true }
  },
  emit: ['ended']
});
Vue.use(Avatar);
Vue.use(Dialog);
Vue.use(Dropdown);
Vue.use(DropdownMenu);
Vue.use(DropdownItem);
Vue.use(Input);
Vue.use(Select);
Vue.use(Option);
Vue.use(Button);
Vue.use(Popover);
Vue.use(Popconfirm);
Vue.use(Tooltip);
Vue.use(Form);
Vue.use(FormItem);
Vue.use(Tag);
Vue.use(Alert);
Vue.use(Icon);
Vue.use(Row);
Vue.use(Col);
Vue.use(Progress);
Vue.use(Link);
Vue.use(Divider);
Vue.use(Image);
Vue.use(DatePicker);
Vue.use(Loading.directive);
Vue.use(WebRTC);
Vue.use(moment);
Vue.use(VueChatScroll);
Vue.component('l-map', LMap);
Vue.component('l-tile-layer', LTileLayer);
Vue.component('l-marker', LMarker);
locale.use(lang);

Vue.prototype.$loading = Loading.service;
Vue.prototype.$notify = Notification;

Vue.config.productionTip = false;
Vue.directive('linkified', linkify);
Vue.filter('locale', localizeFilter);

const firebaseConfig = {
  apiKey: 'AIzaSyBpcfO7rMid3KjF7k-kY7vpzGS4FbaN3IU',
  authDomain: 'vue-light-chat.firebaseapp.com',
  databaseURL: 'https://vue-light-chat.firebaseio.com',
  projectId: 'vue-light-chat',
  storageBucket: 'vue-light-chat.appspot.com',
  messagingSenderId: '83813794850',
  appId: '1:83813794850:web:9d147d3e1caf5718b8f67b'
};

firebase.initializeApp(firebaseConfig);
window.firebase = firebase;
window.io = io;

let app;

firebase.auth().onAuthStateChanged(user => {
  if (!app) {
    store.commit('auth/setCurrentUser', user);
    app = new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount('#app');
  }
});
