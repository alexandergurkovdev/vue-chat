export default {
  namespaced: true,
  state: {
    sideBarShow: true
  },
  actions: {},
  mutations: {
    setSideBarShow(state, payload) {
      state.sideBarShow = payload;
    }
  },
  getters: {
    sideBarShow(state) {
      return state.sideBarShow;
    }
  }
};
