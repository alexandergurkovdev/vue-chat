import locale from '@/filters/localize.filter';

export const loginRules = {
  email: [
    { required: true, message: locale('Input_EmailRequired'), trigger: 'blur' },
    { type: 'email', message: locale('Input_EmailCorrect'), trigger: ['blur', 'change'] }
  ],
  password: [
    { required: true, message: locale('Input_PassRequired'), trigger: 'blur' },
    { min: 6, message: locale('Input_PassCorrect'), trigger: ['blur', 'change'] }
  ]
};
